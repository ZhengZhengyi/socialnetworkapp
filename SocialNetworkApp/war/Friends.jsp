<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link type="text/css" rel="stylesheet" href="/css/main.css" />
<script src="javascript/jquery-3.2.1.min.js"></script>
<title>Friends</title>
<script type="text/javascript">
$(document).ready(function() {
	
	window.fbAsyncInit = function() {
		FB.init({
			appId : '1785120215126652',
			cookie : true, // enable cookies to allow the server to access 
			// the session
			xfbml : true, // parse social plugins on this page
			version : 'v2.11' // use graph api version 2.11
		});

		FB.getLoginStatus(function(response) {
			statusChangeCallback(response);
		});
	};
	
	// Load the SDK asynchronously
	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id))
			return;
		js = d.createElement(s);
		js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	
	function statusChangeCallback(response) {
		if (response.status === 'connected') {
			testAPI();
		} else {
			document.getElementById("status").innerHTML = "Please log into this app.";
		}
	}
	
	function checkLoginState() {
		FB.getLoginStatus(function(response) {
			statusChangeCallback(response);
		});
	}

	function testAPI() {
// 		console.log("connected");
		var id = "";
		FB.api('/me?fields=id,name', function(response) {
			document.getElementById("status").innerHTML = "Hello, " + response.name + " <img class='tw-profile' src='//graph.facebook.com/" + response.id + "/picture' />";			
			console.log("response id: " + response.id);
			id = response.id;
		});
		
		FB.login(function(response) {
		    if (response.authResponse) {
		    	FB.api('me', function(response) {
		    		id = response.id;
		    		FB.api('/'+id+'/friends', function(response) {

						var data = response.data;
						console.log("friends data: " + data);
						$.each(data, function(index, value) {
							$.ajax({
								url: "gettweets",
								dataType: "JSON",
								type: "POST",
								data: {
									userID: value.id,
									userName: value.name,
									userImageURL: "//graph.facebook.com/" + value.id + "/picture"
								},
								success: function(result) {
									$(".tw-container").append(result.data);
									console.log("friends add successfully");
									
									$("ul.tw-content li").unbind("click").click(function(e) {
										e.stopPropagation();
										var key = $(this).attr("data");
										console.log("key on friend: " + key);
										$.get("updatetweet?id=" + key, function(result) {
											console.log("result: " + result);
											var myWindow = window.open("", "_self");
											myWindow.document.write(result);
										})
										return false;
									})
								},
								error: function(jqXHR, exception) {
									console.log("error-list: " + jqXHR.status);
								}
							});
						});
						
		    		})
		    	})
		    }
		}, {scope:'email,user_friends'});
	}
		
})

</script>
</head>
<body>
	<div class="tw-top">
		<a href="Tweet.jsp">Tweet</a> <a href="Friends.jsp">Friends</a> <a
			href="TopTweetsView.jsp">TopTweetsView</a>
		<div class="tw-login">
			<span id="status"></span>
<!-- 			<fb:login-button scope="public_profile,email" onlogin="checkLoginState();"> -->
<!-- 			</fb:login-button> -->
<div onlogin="checkLoginState();" class="fb-login-button" data-max-rows="1" data-size="small" data-button-type="login_with" data-show-faces="false" data-auto-logout-link="true" data-use-continue-as="false"></div>
		</div>
	</div>
	<div class="tw-container">

	</div>
</body>
</html>