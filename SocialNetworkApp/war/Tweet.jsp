<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link type="text/css" rel="stylesheet" href="/css/main.css" />
<script src="javascript/jquery-3.2.1.min.js"></script>
<title>Tweet</title>
<script type="text/javascript">
$(document).ready(function() {
	
	window.fbAsyncInit = function() {
		FB.init({
			appId : '1785120215126652',
			cookie : true, // enable cookies to allow the server to access 
			// the session
			xfbml : true, // parse social plugins on this page
			version : 'v2.11' // use graph api version 2.11
		});

		FB.getLoginStatus(function(response) {
// 			console.log("will create how many times when refresh");
			statusChangeCallback(response);
		});
	};
	
	// Load the SDK asynchronously
	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id))
			return;
		js = d.createElement(s);
		js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	
	function statusChangeCallback(response) {
		if (response.status === 'connected') {
			testAPI();
		} else {
			document.getElementById("status").innerHTML = "Please log into this app.";
		}
	}
	
	function checkLoginState() {
		FB.getLoginStatus(function(response) {
			statusChangeCallback(response);
		});
	}

	function testAPI() {
// 		console.log("connected");
		FB.api('/me?fields=id,name', function(response) {
			document.getElementById("status").innerHTML = "Hello, " + response.name + " <img class='tw-profile' src='//graph.facebook.com/" + response.id + "/picture' />";			

			var userID, userName, userImageURL;
			userID = response.id;
			userName = response.name;
			userImageURL = "//graph.facebook.com/" + response.id + "/picture";
			
			$.ajax({
				url: "gettweets",
				type: "post",
				dataType: "json",
				data: {
					userID: userID,
					userName: userName,
					userImageURL: userImageURL
				},
				success: function(result) {
					$(".tw-user-list").html(result.data);
					$("ul.tw-content li").click(function() {
						$("ul.tw-content li").removeClass("select");
						$(this).addClass("select");
					})
				},
				error: function(jqXHR, exception) {
					console.log("error-list: " + jqXHR.status);
				}
			});
			
			$("#message_btn").click(function() {
				var cnt = $("#message_cont").val();
				if (cnt == "" || cnt == null) {
					alert("Please input some message.");
					return false;
				}
				FB.login(function() {
					FB.api('/me/feed', 'post', {
						message : cnt
					});
				}, {
					scope : 'publish_actions'
				});
			 	
				$.ajax({
					url: "createtweet",
					type: "post",
					dataType: "json",
					data: {
						userID: userID,
						userName: userName,
						userImageURL: userImageURL,
						tweet: cnt
					},
					success: function(result) {
						console.log("result: " + result);
						location.reload();
						alert("create a new tweet successfully.");
					},
					error: function(jqXHR, exception) {
						console.log("error-create: " + jqXHR.status);
					}
				});
			})
		});
		
	}
	
	$("#st_btn").click(function() {
		var $select = $("ul.tw-content li.select");
		if ($select.length <= 0) {
			alert("Please select a tweet to delete.");
		} else {
			FB.ui({
				  method: 'send',
					link: 'https://apps.facebook.com/zheng_sna/updatetweet?id=' + $select.attr("data")
				});
/* 			$.get('https://apps.facebook.com/zheng_sna/tweetdetails?user=0&id' + $select.attr("data"), function(data) {
				console.log("send btn: " + data.error);
			}) */
		}
		
		
	})
	
	$("#dt_btn").click(function() {
		var $select = $("ul.tw-content li.select");
		if ($select.length <= 0) {
			alert("Please select a tweet to delete.");
		} else {
			var key = $select.attr("data");
			$.get("deletetweet?key=" + key, function(result) {
				location.reload();
				alert("Delete successfully.");
			})
		}
	})
	
})

</script>
</head>
<body>
	<div class="tw-top">
		<a href="Tweet.jsp">Tweet</a> <a href="Friends.jsp">Friends</a> <a
			href="TopTweetsView.jsp">TopTweetsView</a>
		<div class="tw-login">
			<span id="status"></span>
<!-- 			<fb:login-button scope="public_profile,email" onlogin="checkLoginState();"> -->
<!-- 			</fb:login-button> -->
<div onlogin="checkLoginState();" class="fb-login-button" data-max-rows="1" data-size="small" data-button-type="login_with" data-show-faces="false" data-auto-logout-link="true" data-use-continue-as="false"></div>
		</div>
	</div>
	<div class="tw-container">
		<div class="tw-message-btn">
			<textarea id="message_cont"></textarea>
			<button id="message_btn" class="tw-tt-btn first">Create & Share tweet</button>
		</div>

		<div class="tw-main">
			<div class="tw-operation-btn">
				<button id="st_btn" class="tw-tt-btn second">Send tweet to friends</button>
				<button id="dt_btn" class="tw-tt-btn third">Delete tweet</button>
			</div>	
			<div class="tw-user-list">
			</div>	
		</div>
	</div>
</body>
</html>
