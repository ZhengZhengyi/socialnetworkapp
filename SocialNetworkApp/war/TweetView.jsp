<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link type="text/css" rel="stylesheet" href="/css/main.css" />
<title>TweetView</title>
</head>
<body>
	<div class="tv-container">
		<c:choose>
			<c:when test="${not empty tweet }">
				<div class="tv-intro"><span>--------A tweet's info below-------</span></div>
				<div class="tv-user">
					<span class="tv-post">Posted by: </span><img src="${tweet.properties.userImageURL}" /> <label>${tweet.properties.userName }</label>
				</div>
				<div class="tv-tweet">
					<span>Tweet Content: </span>
					<div class="tv-content">${tweet.properties.tweet }</div>
				</div>
				<div class="tv-count">
					<span>Visited Counter: </span><label>${tweet.properties.count }</label>
				</div>
			</c:when>
			<c:otherwise>
				<div class="tv-error">No tweets found.</div>
			</c:otherwise>
		</c:choose>
	</div>
</body>
</html>