<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link type="text/css" rel="stylesheet" href="/css/main.css" />
<script src="javascript/jquery-3.2.1.min.js"></script>
<title>TopTweets</title>
<script type="text/javascript">
$(document).ready(function() {
	
	window.fbAsyncInit = function() {
		FB.init({
			appId : '1785120215126652',
			cookie : true, // enable cookies to allow the server to access 
			// the session
			xfbml : true, // parse social plugins on this page
			version : 'v2.11' // use graph api version 2.11
		});

		FB.getLoginStatus(function(response) {
// 			console.log("will create how many times when refresh");
			statusChangeCallback(response);
		});
	};
	
	// Load the SDK asynchronously
	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id))
			return;
		js = d.createElement(s);
		js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	
	function statusChangeCallback(response) {
		if (response.status === 'connected') {
			testAPI();
		} else {
			document.getElementById("status").innerHTML = "Please log into this app.";
		}
	}
	
	function checkLoginState() {
		FB.getLoginStatus(function(response) {
			statusChangeCallback(response);
		});
	}

	function testAPI() {
// 		console.log("connected");
		FB.api('/me?fields=id,name', function(response) {
			document.getElementById("status").innerHTML = "Hello, " + response.name + " <img class='tw-profile' src='//graph.facebook.com/" + response.id + "/picture' />";			

			
			$.ajax({
				url: "toptweets",
				type: "post",
				dataType: "json",
				data: {
					userID: response.id,
					userName: response.name
				},
				success: function(result) {
					console.log("toptweets result: " + result.data);
					$(".tt-content").html(result.data);
				},
				error: function(jqXHR, exception) {
					console.log("error-create: " + jqXHR.status);
				}
			})
			
// 			$.get("toptweets?userID="+response.id, function(result) {
// 				console.log("toptweetsview: " + result);
// 			})
		});
	}
})

</script>
</head>
<body>
	<div class="tw-top">
		<a href="Tweet.jsp">Tweet</a> <a href="Friends.jsp">Friends</a> <a
			href="TopTweetsView.jsp">TopTweetsView</a>
		<div class="tw-login">
			<span id="status"></span>
<!-- 			<fb:login-button scope="public_profile,email" onlogin="checkLoginState();"> -->
<!-- 			</fb:login-button> -->
<div onlogin="checkLoginState();" class="fb-login-button" data-max-rows="1" data-size="small" data-button-type="login_with" data-show-faces="false" data-auto-logout-link="true" data-use-continue-as="false"></div>
		</div>
	</div>
	<div class="tw-container">
		<div class="tt-top-10">This is a popular tweet list of TOP 10</div>
		<div class="tt-content">		
		</div>
	</div>
</body>
</html>
